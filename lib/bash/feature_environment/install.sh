#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd "$SCRIPT_DIR/.."

apt update && apt install -y lsof
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
