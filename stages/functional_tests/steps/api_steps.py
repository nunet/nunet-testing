from contextlib import nullcontext

from behave import given, when, then
from utilities.api_client import APIClient
import os
import json
import re
import jsonschema


@given('the API base URL is "{base_url}"')
def set_api_base_url(context, base_url):
    context.api_client = APIClient(base_url)


@when('a GET request is made to the "{endpoint}" endpoint')
def make_get_request(context, endpoint):
    context.response = context.api_client.get(endpoint)


# @given('I have the POST body from file "{file_path}"')
# def i_have_post_body_from_file(context, file_path):
#     file_path_full = os.path.join(os.getcwd(), file_path)
#     with open(file_path_full, 'r') as file:
#         context.api_data = json.load(file)
#         context.post_body = file.read()

@given('I have the POST body from file "{file_path}"')
def i_have_post_body_from_file(context, file_path):
    file_path_full = os.path.join(os.getcwd(), file_path)

    with open(file_path_full, 'r') as file:
        file_content = file.read().strip()  # Read and strip any surrounding whitespace

        if file_content:  # Check if the file is not empty
            context.api_data = json.loads(file_content)  # Use json.loads to parse from string
            context.post_body = file_content
        else:
            print('I am in else condition')
            context.api_data = ""
            context.post_body = file_content


@when('a POST request is made to the "{endpoint}" endpoint')
def make_post_request(context, endpoint):
    context.response = context.api_client.post(endpoint, context.api_data)


@when('a DELETE request is made to the "{endpoint}" endpoint')
def make_delete_request(context, endpoint):
    # context.response = context.api_client.delete(endpoint)
    # Temp fix due to bug - https://gitlab.com/nunet/device-management-service/-/issues/411
    context.response = context.api_client.post(endpoint, "")


@when('I captured the endpoint result')
def capture_endpoint_result(context):
    print("Capturing endpoint result...")
    context.nunet_full_system_capacity = context.response.json()
    print("Captured nunet_full_system_capacity:", context.nunet_full_system_capacity)
    print('nunet_full_system_capacity = ', context.nunet_full_system_capacity)
    return context.nunet_full_system_capacity


@then('the response status code should be {expected_status_code}')
def verify_status_code(context, expected_status_code):
    expected_status_code = expected_status_code.strip('"\'')
    print('context.response.status_code =', context.response.status_code)
    print('context.response.body =', context.response.reason)
    assert context.response.status_code == int(expected_status_code), \
        f"Expected status code {expected_status_code}, but got {context.response.status_code}"


@then('the response should be in JSON format')
def response_should_be_json(context):
    assert context.response.headers['Content-Type'] == 'application/json', "Response is not in JSON format"


@then('the response status message should be {expected_status_message}')
def verify_status_message(context, expected_status_message):
    expected_status_message = expected_status_message.strip('"\'')
    assert context.response.reason == expected_status_message, \
        f"Expected status message '{expected_status_message}', but got '{context.response.reason}'"


@then('the value of "{attribute}" attribute in the response body should be {expected_value}')
def verify_attribute_value(context, attribute, expected_value):
    response_json = context.response.json()
    actual_value = response_json.get(attribute)
    expected_value = expected_value.strip('"\'')
    if isinstance(actual_value, bool):
        assert actual_value == eval(expected_value), f"Expected value '{expected_value}', but got '{actual_value}'"
    elif isinstance(actual_value, str):
        assert expected_value in actual_value, f"Expected value '{expected_value}', but got '{actual_value}'"
    else:
        raise AssertionError("Unexpected response format")


@then('"{attribute}" attribute in the response body should be non empty')
def verify_attribute_non_empty(context, attribute):
    response_json = context.response.json()
    actual_value = response_json.get(attribute)
    if isinstance(actual_value, str):
        assert len(actual_value) > 0, (f"the value of '{attribute}' attribute in the response body found empty, "
                                      f"while expected mon-empty")
    else:
        assert actual_value is not None


@then('the response should have the following structure')
def verify_response_structure(context):
    expected_schema = json.loads(context.text)
    response_json = context.response.json()
    try:
        jsonschema.validate(instance=response_json, schema=expected_schema)
    except jsonschema.ValidationError as e:
        assert False, f"Response does not match the specified schema: {e}"


@then('each peer in the response should have a non-empty ID of length 46 starting with "Qm" and no special characters')
def verify_non_empty_id(context):
    response_json = context.response.json()

    def validate_peer_id(peer):
        assert peer, "ID should not be empty for each peer"
        # assert len(peer) == 46, f"ID length should be 46 for each peer, but got {len(peer)} for peer {peer}"
        # assert peer.startswith("Qm"), f"ID should start with 'Qm' for each peer, but got {peer}"
        assert re.match("^[a-zA-Z0-9]+$",
                        peer), f"ID should not contain special characters for each peer, but got {peer}"
    # Check if response_json is a list (for peers & peers/dht API) or a dictionary (for peers/self API)
    if isinstance(response_json, list):
        for peer in response_json:
            # Validation of peer ID returned by peers/dht API
            if isinstance(peer, str):
                validate_peer_id(peer)
            else:
                # Validation of peer ID returned by peers API
                id_value = peer.get("ID")
                validate_peer_id(id_value)
    else:
        # Validation of peer ID returned by peers/self API
        id_value = response_json.get("ID")
        validate_peer_id(id_value)


@then('each peer in the response should have a non-empty list of addresses')
def verify_non_empty_addresses(context):
    response_json = context.response.json()

    def validate_addresses(peer):
        assert peer.get("Addrs"), "Addresses should not be empty for each peer"

    # Check if response_json is a list (for peers API) or a dictionary (for peers/self API)
    if isinstance(response_json, list):
        for peer in response_json:
            validate_addresses(peer)
    else:
        validate_addresses(response_json)


@then('each address in the response should be a valid IP address with a port')
def verify_valid_addresses(context):
    response_json = context.response.json()
    ip_port_pattern = re.compile(r'^/ip\d{1,2}/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/(tcp|udp)/\d{1,5}(?:/[a-z]+)?$')

    def is_valid_address(address):
        return bool(ip_port_pattern.match(address))

    def validate_addresses(peer):
        for address in peer.get("Addrs", []):
            assert is_valid_address(address), f"Invalid address format: {address}"

    # Check if response_json is a list (for peers API) or a dictionary (for peers/self API)
    if isinstance(response_json, list):
        for peer in response_json:
            validate_addresses(peer)
    else:
        validate_addresses(response_json)


def calculate_capacity_percentage(percentage, max_capacity):
    return int((percentage / 100) * max_capacity)


def get_payment_addr(context):
    BASE_URL = "http://localhost:9999/api/v1"
    context.api_client = APIClient(BASE_URL)
    make_get_request(context, "onboarding/address/new")
    response_json = context.response.json()
    payment_addr = response_json.get("address")
    return payment_addr


def update_post_body_with_capacity(original_body, memory_percent_value, cpu_percent_value, payment_addr):
    updated_body = json.loads(original_body)
    # Update memory, cpu, and payment_addr
    updated_body["memory"] = memory_percent_value
    updated_body["cpu"] = cpu_percent_value
    updated_body["payment_addr"] = payment_addr
    return json.dumps(updated_body, indent=2)


@when('I update the POST body with {memory_percent:d}% memory and {cpu_percent:d}% cpu')
def prepare_onboarding_body(context, memory_percent, cpu_percent):
    for item in context.linux_system_full_capacity:
        full_memory_capacity = float(item.get("memory"))
        full_cpu_capacity = item.get("cpu")

        print("full_memory_capacity = ", full_memory_capacity)
        print("full_cpu_capacity = ", full_cpu_capacity)

        memory_percent_value = calculate_capacity_percentage(memory_percent, full_memory_capacity)
        cpu_percent_value = calculate_capacity_percentage(cpu_percent, full_cpu_capacity)
        payment_addr = get_payment_addr(context)

        context.post_body = json.dumps(context.api_data, indent=2)

        # Update the POST body with desired onboard capacity
        updated_post_body = update_post_body_with_capacity(
            context.post_body,
            memory_percent_value,
            cpu_percent_value,
            payment_addr
        )
        # Use updated_post_body in your subsequent steps or context as needed
        context.api_data = json.loads(updated_post_body)

        # find the onboarded capacity
        context.onboarded_capacity = {"cpu": cpu_percent_value, "memory": memory_percent_value}


@then('the value of reserved capacity in the response body should be same as onboarded capacity')
def validate_onboarded_capacity(context):
    response_json = context.response.json()
    reserved_capacity = response_json.get("reserved")
    assert reserved_capacity == context.onboarded_capacity, (f'reserved capacity found {reserved_capacity}, '
                                                             f'while expected {context.onboarded_capacity}')
