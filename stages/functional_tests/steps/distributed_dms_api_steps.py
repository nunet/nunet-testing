import requests
from lib.python.behave.lxd import *
from sshtunnel import SSHTunnelForwarder
from behave import given, when, then
import json


def execute_api_call(context, instance, endpoint, method="GET", data=None):
    """Helper function to execute API calls on a given DMS instance."""
    ssh_key_file = context.config.userdata["ssh_key_file"]
    with SSHTunnelForwarder(
        instance,
        ssh_username="root",
        ssh_pkey=ssh_key_file,
        remote_bind_address=("127.0.0.1", 9999),
    ) as server:
        local_endpoint = f"http://localhost:{server.local_bind_port}{endpoint}"
        headers = {'Content-Type': 'application/json'}
        print(f"executing {method} call to {local_endpoint} with data: {data}")

        if method == "GET":
            response = requests.get(local_endpoint, headers=headers)
        elif method == "POST":
            response = requests.post(local_endpoint, data=json.dumps(data), headers=headers)
        elif method == "DELETE":
            # Temp fix due to bug - https://gitlab.com/nunet/device-management-service/-/issues/411
            response = requests.post(local_endpoint, headers=headers)
        else:
            raise ValueError(f"Unsupported HTTP method: {method}")

        print(f"Response from {instance}: {response.status_code}, {response.text}")
        return response

@when('i onboard all the dms instances via api')
def onboard_all_instances_via_api(context):
    instances = get_dms_instances_on_lxd(context.config.userdata["inventory_file"])
    context.responses = {}

    for instance in instances:
        response = execute_api_call(context, instance, "/api/v1/onboarding/onboard", method="POST", data=context.api_data)
        context.responses[instance] = response
        print(f"Response from {instance}: {response.status_code}, {response.text}")

    context.response = context.responses[next(iter(context.responses))]

@when('sending a GET api call to "{endpoint}" on {nth} dms instance')
def send_get_to_endpoint(context, endpoint, nth):
    instances = get_dms_instances_on_lxd(context.config.userdata["inventory_file"])
    instance = instances[int(nth)]
    context.responses = {instance: execute_api_call(context, instance, endpoint)}
    context.response = context.responses[instance]

@when('sending a DELETE api call to "{endpoint}" on {nth} dms instance')
def send_delete_to_endpoint(context, endpoint, nth):
    instances = get_dms_instances_on_lxd(context.config.userdata["inventory_file"])
    instance = instances[int(nth)]
    context.responses = {instance: execute_api_call(context, instance, endpoint, method="DELETE")}
    context.response = context.responses[instance]
