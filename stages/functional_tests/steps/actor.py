from lib.python.behave.lxd import exec_cmd, assert_cmd
from behave import given, then, when
from cli import anchor, grant, delegate
from helpers import (
    get_deployment_logs, set_user_as_root, setup_deployment_capability, setup_user, setup_node, parse_name, parse_name_literal, is_node,
    ensure_node_running, connect_nodes, establish_trust_between_users, deploy, get_deployments, ensemble_is_finished, onboard
)
import random

@given('{org_name} is an organization')
def step_impl(context, org_name):
    org = parse_name(org_name)[0]
    instance = random.choice(context.instances)
    setup_user(context, instance, org)


@given('{user_expr} has a node')
def step_impl(context, user_expr):
    user, node = parse_name(user_expr)

    if not hasattr(context, 'users'):
        context.users = {}

    num_users = len(context.users)
    if "nunet" in context.users:
        num_users -= 1

    # select instance based on user count
    instance_index = min(num_users, len(context.instances) - 1)
    instance = context.instances[instance_index]

    setup_user(context, instance, user)
    setup_node(context, instance, user, node)


#@given('{user_expr} nodes are running')
#def step_impl(context, user_expr):
#    user = parse_name_literal(user_expr)
#
#    assert user in context.users
#    assert context.users[user]["nodes"]
#
#    for node, data in context.users[user]["nodes"].items():
#        instance = data["instance"]
#        if data["running"] is False:
#            out = run(context, instance, user, node)
#            assert_cmd(out)
#
#            out = save_peer_info(context, instance, user, node)
#            assert_cmd(out)


@given('{former_expr} has deployed {file} ensemble on {latter_expr}')
def step_impl(context, former_expr, file, latter_expr):
    former, former_node = parse_name(former_expr)
    latter, latter_node = parse_name(latter_expr)

    # 1. set up capabilities for deployment on both
    setup_deployment_capability(context, "nunet", former, former_node)
    setup_deployment_capability(context, "nunet", latter, latter_node)

    # 2. check if nodes are started
    ensure_node_running(context, former, former_node)
    ensure_node_running(context, latter, latter_node)

    # 3. connect nodes
    connect_nodes(context, former, former_node, latter, latter_node)

    # 4. onboard latter
    onboard(context, latter, latter_node)

    # 5. deploy a job on latter
    deploy(context, former, former_node, latter, latter_node, file)


@given('{org_name} authorizes {user_expr}')
def step_impl(context, org_name, user_expr):
    org = parse_name(org_name)[0]
    user, node = parse_name(user_expr)

    org_instance = context.users[org]["instance"]
    user_instance = context.users[user]["instance"]
    node_instance = context.users[user]["nodes"][node]["instance"]

    user_did = context.users[user]["did"]

    # 1. anchor user as root on dms
    out = anchor(context, node_instance, node, "root", user_did)
    assert_cmd(out)

    # 2. grant organization
    user_grant = grant(context, user_instance, user, org)

    # 3. add granted token as require on dms
    out = anchor(context, node_instance, node, "require", user_grant)
    assert_cmd(out)

    # 4. organization grant the user
    org_token = grant(context, org_instance, org, user)

    # 5. anchor on user as provide
    out = anchor(context, user_instance, user, "provide", org_token)
    assert_cmd(out)

    # 6. delegate to dms
    delegate_token = delegate(context, user_instance, user, node)

    # 7. anchor on dms as provide
    out = anchor(context, node_instance, node, "provide", delegate_token)
    assert_cmd(out)


@given('{former_expr} and {latter_expr} trust each other')
def step_impl(context, former_expr, latter_expr):
    former_user, former_node = parse_name(former_expr)
    latter_user, latter_node = parse_name(latter_expr)

    if former_user == latter_user:
        set_user_as_root(context, former_user, former_node)
    else:
        establish_trust_between_users(context, former_user, former_node, latter_user, latter_node)


@when('{former_expr} calls a behavior on {latter_expr}')
def step_impl(context, former_expr, latter_expr):
    former = parse_name_literal(former_expr)
    latter = parse_name_literal(latter_expr)

    former_user, former_node = parse_name(former_expr)
    latter_user, latter_node = parse_name(latter_expr)

    # start the nodes if they're not running
    if is_node(former):
        ensure_node_running(context, former_user, former_node)
    if is_node(latter):
        ensure_node_running(context, latter_user, latter_node)

    # connect nodes if they're different and both are nodes
    if is_node(former) and is_node(latter):
        connect_nodes(context, former_user, former_node, latter_user, latter_node)

    instance = None
    if is_node(former):
        instance = context.users[former_user]["nodes"][former_node]["instance"]
    else:
        instance = context.users[former]["instance"]

    latter_did = None
    if is_node(latter):
        latter_did = context.users[latter_user]["nodes"][latter_node]["did"]
    else:
        latter_did = context.users[latter_user]["did"]

    out = exec_cmd(context, instance, f"nunet actor cmd --context {former} /public/hello --timeout 10s --dest {latter_did}")
    print(out.stdout)

    # store the output for later verification
    context.last_behavior_output = out


@when('{user_expr} deployment is finished')
def step_impl(context, user_expr):
    user, node = parse_name(user_expr)
    instance = context.users[user]["nodes"][node]["instance"]

    deployments = get_deployments(context, instance, node)
    assert deployments, f"No deployments for {node}"

    for id, status in deployments.items():
        ensemble_is_finished(context, instance, node, id)
    # assert status == "Running", f"Ensemble {id} was still {status}"


@then('{user_expr} should be authorized')
def step_impl(context, user_expr):
    assert hasattr(context, 'last_behavior_output'), "No behavior call output found to verify"
    assert "error" not in context.last_behavior_output.stdout.strip().lower(), f"stdout: {context.last_behavior_output.stdout}"
    assert_cmd(context.last_behavior_output)


@then('{user_expr} should not be authorized')
def step_impl(context, user_expr):
    assert hasattr(context, 'last_behavior_output'), "No behavior call output found to verify"
    if context.last_behavior_output.stdout:
        assert "error" in context.last_behavior_output.stdout.strip().lower(), f"stdout: {context.last_behavior_output.stdout}"
    assert_cmd(context.last_behavior_output, wantErr=True)


@then('{user_expr} ensemble should return "{output}"')
def step_impl(context, user_expr, output):
    user, node = parse_name(user_expr)
    for ensemble in context.users[user]["nodes"][node]["ensembles"]:
        logs = get_deployment_logs(context, user, node, ensemble)
        for alloc, log in logs.items():
            assert output in log, f"{output} not found inside {alloc} log"
