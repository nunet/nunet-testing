Feature: Deployment
  As a Service Provider
  I want to deploy my computation on other nodes
  So that I don't have to use my machine

  Background:
    Given NuNet is an organization
    Given Alice has a node
    And Bob has a node
    And Alice and Bob trust each other

  @wip @capability @deployment @distributed
  Scenario Outline: Retrieve output from execution
    Given Alice has deployed <file> ensemble on Bob
    When Alice deployment is finished
    Then Alice's ensemble should return "Hello from Docker!"

  Examples:
    |          file             |
    | one_alloc.yaml            |
    | two_allocs_one_node.yaml  |
    | two_allocs_two_nodes.yaml |
