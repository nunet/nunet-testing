Feature: Supervision of running ensembles
     As a service provider I want to be able
     To provide supervision strategies for ensemble deployments
     For automatic failure tolerance handling and management


## Please add scenarios below in Gherkin synax
## Or in plain English explaining their logic as comments