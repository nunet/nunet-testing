Feature: Node resources management
    As a compute provider
    I want to be sure that compute nodes that i manage
    Correctly declare their resource availability to the network
    ## Please add scenarios below in Gherkin synax
    ## Or in plain English describing their logic as comments

  Background:
    Given Alice is the compute provider
    And Sam is the computer provider
    And Bob is service provider
    And Alice is trusted by Bob
    And Sam is trusted by Bob
    And Alice and Sam nodes are onboarded
    And Bob requests for a ensemble allocation deployment with resources defined
    And Alice onboarded resources "Alice_resources" are:
      | CPU | GPUs | RAM | Disk |
      |  16 |    4 |  64 |  500 |
    And Sam onboarded resources "Sam_Resources" are:
      | CPU | GPUs | RAM | Disk |
      |   8 |    2 |  32 |  500 |

  @wip @capability @standalone
  Scenario: Successful allocation of single ensemble deployment
    Given Alice with free onboarded resources "Alice_resources"
    When bob requests "one_alloc.yaml" ensemble deployment to Alice
    Then Alice should update the free resources onboarded

  @wip @capability @standalone
  Scenario: Deallocation of resources after ensemble deployment completion
    Given Alice with committed  "one_alloc.yaml"
    When the allocated ensemble is completed
    Then the committed resources should be deallocated

  @wip @capability @standalone
  Scenario: Update committed resources after resource dealloaction
    Given "one_alloc.yaml" is deallocated by Alice
    When Alice updates committed resources to free resources
    Then the committed resources should no longer include "one_alloc.yaml"

  @wip @capability @standalone
  Scenario: Total allocations calculation
    Given Alice has multiple committed "one_alloc.yaml" allocations:
      | AllocationID | CPU | GPUs | RAM | Disk |
      | alloc101     |   4 |    1 |  16 |  100 |
      | alloc102     |   2 |    1 |   8 |   50 |
    When alice computes the total committed allocation resources
    Then it should give:
      | CPU | GPUs | RAM | Disk |
      |   6 |    2 |  24 |  150 |

  @wip @capability @standalone
  Scenario: Updating onboarded resources
    Given Alice with onboarded resources "Alice_resources":
    When onboarded resources are updated to:
      | CPU | GPUs | RAM | Disk |
      |  32 |    8 | 128 | 1000 |
    Then free resources should match the updated onboarded resources

  @wip @capability @standalone
  Scenario: Allocation exceeds available resources
    Given Alice with onboarded resources "Alice_resources":
    When an "one_alloc.yaml" is requested with:
      | CPU | GPUs | RAM | Disk |
      |  16 |    6 |  64 | 1000 |
    Then the "one_alloc.yaml" request should fail with an error

  @wip @capability @standalone
  Scenario: Allocation with zero resources
    Given Alice with onboarded resources "Alice_resources":
    When an "one_alloc.yaml" is requested with:
      | CPU | GPUs | RAM | Disk |
      |   0 |    0 |   0 |    0 |
    Then the "one_alloc.yaml" should succeed without affecting free resources
    # Scenario: Allocating resources on a node with no onboarded resources
    #     Given Alice with 0 onboarded resources "Alice_resources":
    #         | CPU | GPUs | RAM | Disk |
    #         | 0   | 0    | 0   | 0    |
    #     When an "one_alloc.yaml"  is requested with:
    #         | CPU | GPUs | RAM | Disk |
    #         | 4   | 1    | 16  | 100  |
    #     Then the "one_alloc.yaml"  request should fail with an  error
    # Scenario: Free resources exactly match allocation
    #     Given Alice with free resources:
    #         | CPU | GPUs | RAM | Disk |
    #         | 8   | 2    | 32  | 200  |
    #     When an "one_alloc.yaml" is requested with:
    #         | CPU | GPUs | RAM | Disk |
    #         | 8   | 2    | 32  | 200  |
    #     Then the allocation should succeed
    #     And the free resources should be updated to:
    #         | CPU | GPUs | RAM | Disk |
    #         | 0   | 0    | 0   | 0    |

  @wip @capability @standalone
  Scenario: Allocation of resources when all resources are committed
    Given a Alice with free resources:
      | CPU | GPUs | RAM | Disk |
      |   0 |    0 |   0 |    0 |
    And a committed "one_alloc.yaml" with allocationID  "A01":
      | AllocationID | CPU | GPUs | RAM | Disk |
      | A01          |   8 |    2 |  32 |  200 |
    When a new allocation "A02" is requested with:
      | CPU | GPUs | RAM | Disk |
      |   4 |    1 |  16 |  100 |
    Then the allocation "A02" request should fail with an  error

  @wip @capability @distributed
  Scenario: Successful single ensemble deployment of two allocations on two different nodes
    Given Alice and Sam with onboarded resources as "Alice_resources" and "sam_resources":
    When "two_alloc_two_node.yaml" is requested for allocation
    Then the free resources should be updated to
      | Node  | CPU | GPUs | RAM | Disk |
      | Alice |  12 |    3 |  48 |  400 |
      | Bob   |   4 |    1 |  16 |  150 |

  @wip @capability @distributed
  Scenario: Allocation fails due to a single node being unavailable
    Given Alice with onboarded resources as "Alice_resources"
    And Sam with onboarded resources as "sam_resources"
    And Sam is offline
    When "two_alloc_two_node.yaml" is requested for allocation
    Then the allocation should fail with the error

  @wip @capability @distributed
  Scenario: Load balancing across nodes
    Given Alice and Sam with onboarded resources as "Alice_resources" and "sam_resources"
    And Alice has free resources:
      | CPU | GPUs | RAM | Disk |
      |   4 |    1 |  16 |  100 |
    And Sam has free resources:
      | CPU | GPUs | RAM | Disk |
      |   8 |    2 |  32 |  200 |
    When an allocation "two_alloc_two_node.yaml"  are provisioned with:
      | AllocationId | CPU | GPUs | RAM | Disk |
      | A01          |   4 |    1 |  16 |  100 |
      | A02          |   4 |    1 |  16 |  100 |
    Then the resource manager should allocate resources respectively
    # 1.3

  @wip @capability @distributed
  Scenario: Successful single ensemble deployment with two allocations on the same node
    Given Alice with onboarded resources "Alice_resources"
    When "two_alloc_node.yaml" are requested for deployment
    Then the committed resources should include the new allocations "two_alloc_node.yaml"

  @wip @capability @distributed
  Scenario: Allocation fails due to insufficient resources
    Given Alice with onboarded resources "Alice_resources"
    And free resources are:
      | CPU | GPUs | RAM | Disk |
      |   4 |    1 |  16 |  100 |
    When "two_alloc_node.yaml" is requested for deployment provisioned with:
      | AllocationId | CPU | GPUs | RAM | Disk |
      | A01          |   4 |    1 |  16 |  100 |
      | A02          |   4 |    1 |  16 |  100 |
    Then the allocation should fail with the error
