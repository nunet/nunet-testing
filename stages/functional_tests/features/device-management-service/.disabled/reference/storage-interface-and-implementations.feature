@wip
Feature: Storage Interface and Implementations
   An allocations and their executors can read and write data to remote storate
   Via unified storage interface 
   And pluggable interfaces for different storage backends / providers

# all executors are able to read and write data to the provided storage, as allowed and via the interface;
# DMS currently supports S3 storage backend

