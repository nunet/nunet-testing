@wip
Feature: Local Access via CLI API
   DMS exposes a single protected CLI API endpoint
   For consuming all commands and queries
   Via a local network interface

# api responds only to locally issued commands and queries
# all commands should be authenticated with capability tokens


