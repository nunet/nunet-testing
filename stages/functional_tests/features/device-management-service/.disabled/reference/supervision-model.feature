@wip
Feature: Supervision Model
    nuActor system implements Erlant/OTP inspired supervision model
    for control flow and fault tolerance of ensemble executions

# Ability to build a 'decentralized' control plane on NuNet;
# error propagation between Actors participating in the same compute workflow;
# heartbeat and health-check functionalities;
# conceptually, supervisor model enables failure recovery and fault tolerance features in the network;
