#### Purpose and audience

The development environment is composed of a somewhat larger (than feature) network of heterogeneous devices sourced from the community. Since NuNet, as a decentralized network, will not have control of the devices sourced from community, the development environment will encompass communication channels with the community members who will participate in the [NuNet testers program](https://discord.gg/CWrVfxSA).

#### CI/CD stages

Branch: `develop` branch

Develop environment is used to run the following CI/CD pipeline stages according to the pre-defined schedule, to be communicated to community testers:

- [static analysis](NuNet-CI-pipeline#static-analysis-code-quality-name-static_analysis);
- [unit tests](NuNet-CI-pipeline#unit-tests-name-unit_tests);
- [static security tests](NuNet-CI-pipeline#static-security-tests-name-security_tests_1);
- [build](NuNet-CI-pipeline#build-name-build) (if needed by the feature);
- [functional tests / API tests](NuNet-CI-pipeline#functional-tests-api-tests-name-functional_tests) (if needed by the feature);
- [security tests](NuNet-CI-pipeline#automated-security-tests-name-security_tests_2)
- [automatic regression tests](https://gitlab.com/nunet/documentation/-/wikis/NuNet-CI-pipeline#regression-tests-name-regression_tests)

#### Architecture

The development environment contains:
- virtual machines and containers hosted in NuNet cloud servers;
- machines owned by NuNet team members;
- machines provided by the community members on constant basis via [NuNet Network private testers](https://gitlab.com/nunet/documentation/-/wikis/nunet-network-private-testers) program;

#### Triggering schedule

The CI/CD pipeline in development environment is triggered in two cases:

1) automatically when [a merge request](https://gitlab.com/groups/nunet/-/merge_requests?scope=all&state=opened) is approved and merged into the `develop`branch (see all merged;
2) according to the pre-determined schedule for running stages that are more heavy on compute requirements -- which ideally may include the more advanced stages ; depending on the speed of development, NuNet may be schedule weekly or nightly builds and runs of the platform with the full pipeline (possibly including the latest stages of the CI/CD pipeline normally reserved for Staging environment only). In principle, Development environment should be able to run **all** automatic tests.

