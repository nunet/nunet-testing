# Staging environment

*Note: Transferred from old Wiki [page](https://gitlab.com/nunet/documentation/-/wikis/NuNet-test-process-and-environments); to be updated as per new developments.* 

## Purpose and public

Testnet is this network and is used by developers, QA/security engineers and community testers. Manged by the Product Owner.

## CI/CD stages

Branch: staging branch, created from developby freezing features scheduled for release;

CI/CD pipeline runs the following stages automatically as well as manually where required:

* static analysis;
* unit tests;
* static security tests;
* build (if needed by the feature);
* functional tests / API tests (if needed by the feature);
* security tests
* regression tests
* performance and load tests
* live security tests

## Architecture

The Staging environment contains:

* virtual machines and containers hosted in NuNet cloud servers
* machines owned by NuNet and NuNet team members
* extensive network of community testers' machines/devices provided via NuNet Network private testers, covering all possible configurations of the network and most closely resembling the actual NuNet network in production:
    * with different hardware devices
    * owned by separate individuals and entities
    * connected to internet by different means:
        - having IP addresses
        - behind different NAT types
        - having different internet speeds
        - having different stability of connection
        - etc

## Triggering schedule

Testing on staging environment is triggered manually as per platform life-cycle and release schedule. When the staging branch is created from develop branch with the frozen features ready for release, the following actions are performed:

1. The staging environment / testnet is constructed by inviting community testers to join their machines in order to cover architecture described above;
1. All applications are deployed on the network (as needed) in preparation for automatic and manual regression testing and load testing;
1. Manual testing schedule is released and communicated to community testers;
1. CI/CD pipelsine is triggered with all automatic tests and manual tests;
1. Bug reports are collected and resolved;
1. Manual tests resulting in bugs are automated and included into CI/CD pipeline;

The above cycle is repeated until no bugs are observed. When this happens, the staging branch is marked for release into production environment.