# 2. Feature environment

Date: 2024-05-24

## Status

Accepted

Expanded by [3. ssh for running remote commands](0003-ssh-for-running-remote-commands.md)

## Context

Based on the test-matrix available at <https://gitlab.com/nunet/test-suite>, there was a need to formalize the setup of separate environments that would run
DMS against specific functional tests. Each environment would have certain characteristics like Distro, CPU Architecture and, in some cases, GPUs, necessary
to run those tests. For documentation about the environments, see <https://gitlab.com/nunet/test-suite/-/tree/develop/environments?ref_type=heads>.

The work for feature environment itself is described at <https://gitlab.com/nunet/test-suite/-/issues/81> and the actual implementation context can be found
at <https://gitlab.com/nunet/test-suite/-/merge_requests/91>. For the feature environment itself, sole pre-requisite was for each host to be geographically
separated from each other.

## Decision

We will use **LXD** for virtual-machine management, **terraform/opentofu** to setup and teardown the environments, **gitlab ci** to orchestrate the
deployments and run the tests and**nebula** as the backbone for the overlay network, necessary to provide access to hosts in private networks. Each
particular piece of the architecture should be decoupled from each other.

## Consequences

By decoupling and making each part naive or building upon each other, we make the implementation both more flexible but the integration not seamless. That is
anyone with a particular infrastructure can use the terraform and accompanying scripts to deploy a viable test environment locally. However, when integrating
the solution with the Gitlab CI pipeline, by not having each resulting virtual machine as a registered Gitlab runner, we have to fall back to the lxc cli to
pull relevant files back into the CI pipeline so that they can be exposed as artifacts.

