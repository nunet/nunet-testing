#!/usr/bin/env bash
# Main provisioning script for DMS on LXD infrastructure
# 
# Execution flow:
# 1. Sources lib.sh for common functions
# 2. Handles Docker runner if enabled via DMS_ON_LXD_DOCKER
# 3. Generates SSH key pair if not present
# 4. Runs build.sh which:
#    - Initializes terraform
#    - Generates config files based on DMS_ON_LXD_ENV
#    - Tests and adds LXD remote hosts
#    - Creates terraform plan
# 5. Applies terraform plan
# 6. Outputs vm addresses to lxd_vm_addresses.txt
# 7. Waits for DMS to be ready on all instances
#
# Dependencies:
# - lib.sh: Common functions
# - build.sh: Infrastructure setup
# - remote-wait-for-dms.sh: DMS readiness check
# Files created:
# - lxd-key/lxd-key.pub: SSH keypair
# - lxd_vm_addresses.txt: List of provisioned instances

set -euo pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $SCRIPT_DIR/lib.sh

handle-docker-runner make.sh

if ! [[ -f lxd-key ]]; then
    ssh-keygen -t ed25519 -C "temp lxd key" -f lxd-key -N ""
else
    echo lxd-key already exists
fi

source build.sh
bash apply.sh
terraform output -json | jq -cr '.lxd_vm_addresses.value[]' > lxd_vm_addresses.txt

for instance in $(cat lxd_vm_addresses.txt); do
  echo waiting for dms to be read on instance $instance...
  TARGET_INSTANCE=$instance bash remote-wait-for-dms.sh
done

