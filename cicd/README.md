# CICD

This folder contains job definitions for the cicd pipeline that implements the test matrix described in [this project's readme](README.md).

# Usage

You need to import this project's `cicd/Auto-Devops.gitlab-ci.yml` and define a base image that will server as the default image for building the project:

```yaml
include:
  project: nunet/test-suite
  file: cicd/Auto-DevOps.gitlab-ci.yml
  ref: release

image: registry.gitlab.com/GROUP/PROJECT/BUILDER-IMAGE:latest
```

There are multiple variables that are used to disable specific parts of the pipeline:

```yaml
variables: 
  BUILD_DISABLED: "yes"
  CODE_QUALITY_DISABLED: true
  SKIP_UNIT_TESTS: true
  DEFECTDOJO_DISABLED: true
  CONTAINER_SCANNIG_DISABLED: true
  SAST_DISABLED: true
  SECRET_DETECTION_DISABLED: true
  SEMVER_DISABLED: true
  SONARQUBE_DISABLED: true
  FEATURE_ENVIRONMENT_DISABLED: true
```

For later stages to work, like functional tests, you need to define a build job called `Build`.

# Stages

These are the implemented stages of the pipeline. Please refer to [stages](stages) for the description and definition of each stage's functions.

## Code quality

This stage contains jobs that control code quality using linters and analysers. Currently we implement Code Climate for general languages and golangci-lint for golang projects.

The golangci-lint specifically supports definition of rules and settings in a file called `.golanci.yml`. It also suports toml and json formats. For more information, please
refer to the tool's [configuration guide](https://golangci-lint.run/usage/configuration/).

## Unit Tests

## Security Tests 1

## Build

This stage builds artifacts necessary for further testing and distribution.

## Functional Tests

# Testing

Tests are described in the folder `tests/`. It contains high level functionality tests for the cicd pipeline that can either be triggered
manually or with dummy merge requests, as needed.

